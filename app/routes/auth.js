const express = require('express');
const router = express.Router();

const mongoose = require('mongoose');
const { Schema, model } = require('mongoose');

const jwt = require('jsonwebtoken');

mongoose.connect('mongodb://localhost:27017/jwt', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => console.log('Đã kết nối tới MongoDB'))
    .catch(err => console.error('Lỗi kết nối MongoDB:', err));
const userSchema = new Schema({
    username: { type: String, required: true },
    password: { type: String, required: true }
});
const User = model('User', userSchema);

router.get('/signup/:username/:password', async (req, res) => {
    try {
        const newUser = new User({ username: req.params.username, password: req.params.password });
        await newUser.save();
        res.send(newUser);
    } catch (err) {
        res.send(err);
    }
});

router.get('/login/:username/:password', async (req, res) => {
    try {
        const { username, password } = req.params;
        if (username !== null && password !== null) {
            const user = await User.findOne({ username, password });
            if (user){
                const token = jwt.sign({ username: user.username }, 'ABC');
                res.cookie('token', token, { maxAge: 3600000, httpOnly: true });
                res.send('login success');
            }
            else
                res.send('user not found');
        }
    }catch(err){
        res.send(err);
    }
});

module.exports = router;