const express = require('express');
const app = express();

const authRoute=require('./routes/auth');

app.use('/auth',authRoute);


app.listen(80,()=>{
    console.log('App listening...');
});
